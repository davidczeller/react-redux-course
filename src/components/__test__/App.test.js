import React from 'react'
import {
  shallow
} from 'enzyme'

import App from 'components/App'
import CommentBox from 'components/CommentBox'
import CommentList from 'components/CommentList'



let wrapped;

beforeEach(() => {
  wrapped = shallow( < App/> )
})

it('shows a comment box', () => {
  //wrapped could be component so that way makes more sense
  // const wrapped = shallow( < App/> )  
  expect(wrapped.find(CommentBox).length).toEqual(1) //wrapped.find() returns an array
})

it('shows comment list',  () => {
  expect(wrapped.find(CommentList).length).toEqual(1)  
})