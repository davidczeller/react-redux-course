import React from 'react'

import { mount } from 'enzyme'

import Root from 'Root'
import CommentBox from 'components/CommentBox'

let wrapper;

beforeEach(() => {
  wrapper = mount(
    <Root>
      <CommentBox />
    </Root>
  )
})

afterEach(() => {
  wrapper.unmount()
})

it('has a text area and two buttons', () => {
  expect(wrapper.find('textarea').length).toEqual(1)
  expect(wrapper.find('button').length).toEqual(2)
})

describe('the textarea', () => {
  beforeEach(() => {
    wrapper.find('textarea').simulate('change', {
      target: {
        value: 'new comment'
      }
    })
    wrapper.update()
  })
  it('has a working textarea with text', () => {
    // wrapper.find('textarea').simulate('change', {
    //   target: {
    //     value: 'new comment'
    //   }
    // })
    // wrapper.update() // forcing the DOM to rereender
    expect(wrapper.find('textarea').prop('value')).toEqual('new comment')
  })

  it('has a working textarea which is empty', () => {
    // wrapper.find('textarea').simulate('change', {
    //   target: {
    //     value: 'new comment'
    //   }
    // })
    // wrapper.update()
    // expect(wrapper.find('textarea').prop('value')).toEqual('new comment')
    wrapper.find('form').simulate('submit')
    wrapper.update()
    expect(wrapper.find('textarea').prop('value')).toEqual('')
  })
})