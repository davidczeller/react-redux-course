import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import * as actions from 'actions'

import 'components/CommentBox.scss'

import classNames from 'classnames';
import { Button, Typography } from '@material-ui/core';


class CommentBox extends PureComponent {
  state = {
    isLoading: false,
    comment: '',
  }

  handleChange = (e) => {
    this.setState({ comment: e.target.value })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.saveComment(this.state.comment)
    this.setState({ comment: '' })
  }

  isFetching = () => {
    // if there is no comment in the list then
  }

  render() {
    const { isLoading } = this.state;
    console.log(isLoading, this.state)
    return (
      <div className='container'>
        <form /*onSubmit={this.handleSubmit}*/>
          <Typography variant='h4' className='title'>Add a comment</Typography>
          <textarea
            value={this.state.comment}
            onChange={this.handleChange}
            className='textarea'
          />
          <div>
          </div>
        </form>
        <div className='buttonContainer'>
          <Button
            className='button'
            variant="contained"
            onClick={this.handleSubmit}
          >
            Submit
        </Button>
          <Button
            className={classNames('button', 'fetchBtn')}
            variant="contained"
            onClick={this.props.fetchComments}
          >
            Fetch Comments
        </Button>
        </div>
      </div>
    )
  }
}

export default connect(null, actions)(CommentBox);