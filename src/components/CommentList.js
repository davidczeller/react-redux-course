import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { Paper, Typography } from '@material-ui/core';

import 'components/CommentList.scss'

class commentList extends PureComponent {
  renderComments() {
    return this.props.comments.map((comment, index) => {
      return (
        <Paper elevation={3} className='paper' key={`${index} ${comment}`}>
          <Typography variant='body2' className='comment'>{comment}</Typography>
        </Paper>
      )
    })
  }
  render() {
    return (
      <div className='list'>
        {this.renderComments()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { comments: state.comments }
}

export default connect(mapStateToProps)(commentList)